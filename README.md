# Symfony Full skeleton
    
- PHPUnit configuration for unit, integration and functional tests
- Doctrine Fixtures bundle
- /build/ folder ignored
- Setup .env.test
- Setup .gitlab-ci.yml with MySQL service
- Add Docker image for project registry